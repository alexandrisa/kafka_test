package com.isaev;

import com.isaev.matrix.MatrixIsland;
import javafx.util.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

public class NeighbourhoodFinderTest {

    @Test
    public void test1() {
        int[][] data = {
                {1, 0, 0, 1, 1, 0, 1},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0}
        };
        List<MatrixIsland> islands = NeighbourhoodFinder.findIslands(data);
        Assertions.assertEquals(islands.size(), 3);

        MatrixIsland island1 = islands.get(0);
        Set<Pair<Integer, Integer>> coordinates = island1.getCoordinates();
        Assertions.assertEquals(coordinates.size(), 1);
        Assertions.assertTrue(coordinates.contains(new Pair<>(0, 0)));

        MatrixIsland island2 = islands.get(1);
        Set<Pair<Integer, Integer>> coordinates2 = island2.getCoordinates();
        Assertions.assertEquals(coordinates2.size(), 2);
        Assertions.assertTrue(coordinates2.contains(new Pair<>(0, 3)));
        Assertions.assertTrue(coordinates2.contains(new Pair<>(0, 4)));

        MatrixIsland island3 = islands.get(2);
        Set<Pair<Integer, Integer>> coordinates3 = island3.getCoordinates();
        Assertions.assertEquals(coordinates3.size(), 1);
        Assertions.assertTrue(coordinates3.contains(new Pair<>(0, 6)));

    }

    @Test
    public void test2() {
        int[][] data = {
                {0, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {0, 1, 0, 1, 1, 1},
                {0, 0, 0, 1, 1, 1}
        };
        List<MatrixIsland> islands = NeighbourhoodFinder.findIslands(data);
        Assertions.assertEquals(islands.size(), 2);

        MatrixIsland island1 = islands.get(0);
        Set<Pair<Integer, Integer>> coordinates = island1.getCoordinates();
        Assertions.assertEquals(coordinates.size(), 5);
        Assertions.assertTrue(coordinates.contains(new Pair<>(1, 1)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(2, 0)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(2, 1)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(2, 2)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(3, 1)));

        MatrixIsland island2 = islands.get(1);
        Set<Pair<Integer, Integer>> coordinates2 = island2.getCoordinates();
        Assertions.assertEquals(coordinates2.size(), 6);
        Assertions.assertTrue(coordinates2.contains(new Pair<>(3, 3)));
        Assertions.assertTrue(coordinates2.contains(new Pair<>(3, 4)));
        Assertions.assertTrue(coordinates2.contains(new Pair<>(3, 5)));
        Assertions.assertTrue(coordinates2.contains(new Pair<>(4, 3)));
        Assertions.assertTrue(coordinates2.contains(new Pair<>(4, 4)));
        Assertions.assertTrue(coordinates2.contains(new Pair<>(4, 5)));
    }

    @Test
    public void test3() {
        int[][] data = {
                {0, 0, 0, 0, 0, 0, 0},
                {0, 1, 1, 1, 0, 0, 0},
                {0, 1, 0, 1, 0, 0, 0},
                {0, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0}
        };
        List<MatrixIsland> islands = NeighbourhoodFinder.findIslands(data);
        Assertions.assertEquals(islands.size(), 1);

        MatrixIsland island1 = islands.get(0);
        Set<Pair<Integer, Integer>> coordinates = island1.getCoordinates();
        Assertions.assertEquals(coordinates.size(), 8);
        Assertions.assertTrue(coordinates.contains(new Pair<>(1, 1)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(1, 2)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(1, 3)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(2, 1)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(2, 3)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(3, 1)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(3, 2)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(3, 3)));
        }

    @Test
    public void test4() {
        int[][] data = {
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 0, 1, 1},
                {0, 1, 0, 0, 0, 1, 0}
        };
        List<MatrixIsland> islands = NeighbourhoodFinder.findIslands(data);
        Assertions.assertEquals(islands.size(), 2);

        MatrixIsland island = islands.get(0);
        Set<Pair<Integer, Integer>> coordinates = island.getCoordinates();
        Assertions.assertEquals(coordinates.size(), 5);
        Assertions.assertTrue(coordinates.contains(new Pair<>(2, 5)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(2, 6)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(3, 5)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(3, 6)));
        Assertions.assertTrue(coordinates.contains(new Pair<>(4, 5)));

        MatrixIsland island2 = islands.get(1);
        Set<Pair<Integer, Integer>> coordinates2 = island2.getCoordinates();
        Assertions.assertEquals(coordinates2.size(), 3);
        Assertions.assertTrue(coordinates2.contains(new Pair<>(3, 0)));
        Assertions.assertTrue(coordinates2.contains(new Pair<>(3, 1)));
        Assertions.assertTrue(coordinates2.contains(new Pair<>(4, 1)));

    }

}
