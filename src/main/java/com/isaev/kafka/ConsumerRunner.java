package com.isaev.kafka;

import com.isaev.NeighbourhoodFinder;
import com.isaev.matrix.IntMatrix;
import com.isaev.matrix.MatrixIsland;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.time.Duration;
import java.util.List;

public class ConsumerRunner {

    public static Integer MAX_NO_MESSAGE_FOUND_COUNT = 100;

    public static void main(String[] args) {
        runConsumer();
    }

    private static void runConsumer() {
        Consumer<Long, IntMatrix> consumer = new ConsumerFactory<IntMatrix>().createConsumer();
        int noMessageFound = 0;
        while (true) {
            ConsumerRecords<Long, IntMatrix> consumerRecords = consumer.poll(Duration.ofMillis(1000));
            // 1000 is the time in milliseconds consumer will wait if no record is found at broker.
            if (consumerRecords.count() == 0) {
                noMessageFound++;
                if (noMessageFound > MAX_NO_MESSAGE_FOUND_COUNT)
                    // If no message found count is reached to threshold exit loop.
                    break;
                else
                    continue;
            }

            //print each record result.
            consumerRecords.forEach(record -> {
                System.out.println("Record Key " + record.key());
                System.out.println("Record value " + record.value());
                System.out.println("Record partition " + record.partition());
                System.out.println("Record offset " + record.offset());
                IntMatrix value = record.value();
                if (value != null
                        && value.getData().length > 0) {
                    List<MatrixIsland> matrixIslands = NeighbourhoodFinder.findIslands(value.getData());
                    System.out.println("matrixIslands = " + matrixIslands);
                }
            });
            // commits the offset of record to broker.
            consumer.commitAsync();
        }
        consumer.close();
    }
}
