package com.isaev.kafka;

import com.isaev.matrix.IntMatrix;
import com.isaev.matrix.RandomMarixCreator;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.concurrent.ExecutionException;

public class ProducerRunner {
    public static Integer MESSAGE_COUNT = 1;


    public static void main(String[] args) {
        runProducer();
    }

    private static void runProducer() {
        Producer<Long, IntMatrix> producer = new ProducerFactory<IntMatrix>().createProducer();
        for (int index = 0; index < MESSAGE_COUNT; index++) {
            IntMatrix intMatrix = new IntMatrix(RandomMarixCreator.createRandomMatrix(1000 * (index + 1), 1000 * (index + 1)));
            ProducerRecord<Long, IntMatrix> record = new ProducerRecord<>(ConsumerFactory.TOPIC_NAME, intMatrix);
            try {
                RecordMetadata metadata = producer.send(record).get();
                System.out.println("Record sent with key " + index + " to partition " + metadata.partition()
                        + " with offset " + metadata.offset());
            } catch (ExecutionException e) {
                System.out.println("Error in sending record");
                System.out.println(e);
            } catch (InterruptedException e) {
                System.out.println("Error in sending record");
                System.out.println(e);
            }
        }
    }
}
