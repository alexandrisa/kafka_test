package com.isaev;

import com.isaev.matrix.MatrixIsland;
import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NeighbourhoodFinder {

    @NotNull
    public static List<MatrixIsland> findIslands(@NotNull int[][] data) {
        if (data == null) {
            return Collections.emptyList();
        }

        List<MatrixIsland> matrixIslands = new ArrayList<>();
        for (int i = 0; i < data.length; i++) {
            int[] row = data[i];
            for (int j = 0; j < row.length; j++) {
                int value = row[j];
                if (value > 0) {
                    MatrixIsland matrixIsland = new MatrixIsland();
                    matrixIslands.add(matrixIsland);
                    addNeighbourhoods(matrixIsland, data, i, j);
                }
            }
        }
        return matrixIslands;
    }

    private static void addNeighbourhoods(MatrixIsland matrixIsland, int[][] data, int i, int j) {
        matrixIsland.addPoint(i, j);
        data[i][j] = 0;

        addLeftNeighbourhood(matrixIsland, data, i, j);
        addRightNeighbourhood(matrixIsland, data, i, j);
        addTopNeighbourhood(matrixIsland, data, i, j);
        addDownNeighbourhood(matrixIsland, data, i, j);
    }

    private static void addDownNeighbourhood(MatrixIsland matrixIsland, int[][] data, int i, int j) {
        if (i < 0
                || j < 0
                || i == data.length - 1
                || j > data[i + 1].length - 1) {
            return;
        }

        int value = data[i + 1][j];
        if (value > 0) {
            addNeighbourhoods(matrixIsland, data, i + 1, j);
        }
    }

    private static void addTopNeighbourhood(MatrixIsland matrixIsland, int[][] data, int i, int j) {
        if (i < 0
                || j < 0
                || i == 0
                || j > data[i - 1].length - 1) {
            return;
        }

        int value = data[i - 1][j];
        if (value > 0) {
            addNeighbourhoods(matrixIsland, data, i - 1, j);
        }
    }

    private static void addRightNeighbourhood(MatrixIsland matrixIsland, int[][] data, int i, int j) {
        if (i < 0
                || j < 0
                || data[i].length == 0
                || j == data[i].length - 1) {
            return;
        }
        int value = data[i][j + 1];
        if (value > 0) {
            addNeighbourhoods(matrixIsland, data, i, j + 1);
        }
    }

    private static void addLeftNeighbourhood(MatrixIsland matrixIsland, int[][] data, int i, int j) {
        if (i < 0
                || j < 0
                || data[i].length == 0
                || j == 0) {
            return;
        }
        int value = data[i][j - 1];
        if (value > 0) {
            addNeighbourhoods(matrixIsland, data, i, j - 1);
        }
    }

}
