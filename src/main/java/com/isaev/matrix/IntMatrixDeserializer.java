package com.isaev.matrix;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;


public class IntMatrixDeserializer implements Deserializer<IntMatrix> {

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
    }
    @Override
    public IntMatrix deserialize(String topic, byte[] data) {
        ObjectMapper mapper = new ObjectMapper();
        IntMatrix object = null;
        try {
            object = mapper.readValue(data, IntMatrix.class);
        } catch (Exception exception) {
            System.out.println("Error in deserializing bytes "+ exception);
        }
        return object;
    }
    @Override
    public void close() {
    }
}