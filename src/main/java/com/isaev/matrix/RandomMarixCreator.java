package com.isaev.matrix;

import java.util.Random;

public class RandomMarixCreator {

    public static int[][] createRandomMatrix(int n, int m) {
        int[][] matrix = new int[n][m];
        Random random = new Random();
        int i, j;
        for (i = 0; i < n; i++) {
            for (j = 0; j < m; j++) {
                matrix[i][j] = random.nextInt(256) % 3 > 0 ? 0 : 1;
            }
        }
        return matrix;
    }
}
