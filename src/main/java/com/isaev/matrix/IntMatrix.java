package com.isaev.matrix;

import java.util.Arrays;

public class IntMatrix {

    private int[][] data;

    public IntMatrix() {
    }

    public IntMatrix(int[][] data) {
        this.data = data;
    }

    public int[][] getData() {
        return data;
    }

    public void setData(int[][] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("IntMatrix{" +
                "data=" ).append("\n");
        for (int[] datum : data) {
            sb.append(Arrays.toString(datum))
            .append("\n");

        }
        sb.append("}");
        return sb.toString();
    }
}
