package com.isaev.matrix;

import javafx.util.Pair;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MatrixIsland {

    private Set<Pair<Integer, Integer>> coordinates = new HashSet<>();

    public Set<Pair<Integer, Integer>> getCoordinates() {
        return Collections.unmodifiableSet(coordinates);
    }

    public boolean addPoint(int n, int m) {
        return coordinates.add(new Pair<>(n, m));
    }

    @Override
    public String toString() {
        return "MatrixIsland{" +
                "coordinates=" + coordinates +
                '}';
    }
}
